#!/bin/bash

MODEL=basic_model
echo "# Basic model -------------------------------------------------------"
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_a -bg 48 -video /home/felix/Documents/motion-detection/data/sample_a.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_b -bg 48 -video /home/felix/Documents/motion-detection/data/sample_b.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_c -bg 48 -video /home/felix/Documents/motion-detection/data/sample_c.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_d -bg 48 -video /home/felix/Documents/motion-detection/data/sample_d.mp4

MODEL=parametric_model
echo "# Parametric model (Simple Gaussian) --------------------------------"
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_a -bg 48 -t 0.0025 -video /home/felix/Documents/motion-detection/data/sample_a.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_b -bg 48 -t 0.002 -video /home/felix/Documents/motion-detection/data/sample_b.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_c -bg 48 -t 0.003 -video /home/felix/Documents/motion-detection/data/sample_c.mp4
./build/src/$MODEL/$MODEL -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_d -bg 48 -t 0.005 -video /home/felix/Documents/motion-detection/data/sample_d.mp4

echo "# Parametric model (Gaussian Mixture Model) --------------------------------"
./build/src/$MODEL/$MODEL -gmm -si 10 -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_a -bg 48 -t 0.1 -video /home/felix/Documents/motion-detection/data/sample_a.mp4
./build/src/$MODEL/$MODEL -gmm -si 20 -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_b -bg 48 -t 0.35 -video /home/felix/Documents/motion-detection/data/sample_b.mp4
./build/src/$MODEL/$MODEL -gmm -si 7 -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_c -bg 48 -t 0.25 -video /home/felix/Documents/motion-detection/data/sample_c.mp4
./build/src/$MODEL/$MODEL -gmm -si 10 -save -o /home/felix/Documents/motion-detection/tests/$MODEL+_sample_d -bg 48 -t 0.2 -video /home/felix/Documents/motion-detection/data/sample_d.mp4