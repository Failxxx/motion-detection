#include "../include/App.hpp"

/// Return options defined in the m_args attribute
std::vector<std::string> App::get_options() {
	std::vector<std::string> options;
	for (auto& arg : get_args()) {
		options.push_back(arg.first);
	}

	return options;
}


/// Read the current frame
void App::read_frame(cv::Mat& frame) {
	if (m_is_movie) {
		if (!m_movie.get_video().read(frame)) {
			std::cerr << "Error reading the current frame (movie)." << std::endl;
			m_run = false;
		}
	}
	else {
		if (!m_camera.get_video().read(frame)) {
			std::cerr << "Error reading the current frame (camera)." << std::endl;
			m_run = false;
		}
	}
}


/// Get the video (adapats whether is is a movie or not)
cv::VideoCapture App::get_video() {
	if (m_is_movie) {
		return m_movie.get_video();
	}
	else {
		return m_camera.get_video();
	}
}


/// Initialize output videos
bool App::initialize_outputs(const std::string file_name, const int& fps, const bool& color, const char fcc[4]) {
	const auto width = get_video().get(cv::CAP_PROP_FRAME_WIDTH);
	const auto height = get_video().get(cv::CAP_PROP_FRAME_HEIGHT);
	const auto size = cv::Size(width, height);
	const auto fourcc = cv::VideoWriter::fourcc(fcc[0], fcc[1], fcc[2], fcc[3]);

	return m_output.open(file_name + ".mp4", fourcc, fps, size, color) && m_output_foreground.open(file_name + "_foreground.mp4", fourcc, fps, size, color);
}


/// Not the best I think, but it works.
/// Read an argument typed by the user
template<typename T>
T App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	throw NotImplementedException();
}

template<>
std::string App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	if (args.at(option).second == "") {
		// If optional, return default value
		if (get_args().at(option).first == App::Arg::OPTIONAL) {
			return get_args().at(option).second.second;
		}
		else {
			throw std::invalid_argument("Option ('" + option + "') is required.");
		}
	}
	else {
		return args.at(option).second;
	}
}

template<>
int App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	if (args.at(option).second == "") {
		// If optional, return default value
		if (get_args().at(option).first == App::Arg::OPTIONAL) {
			return std::atoi(get_args().at(option).second.second.c_str());
		}
		else {
			throw std::invalid_argument("Option ('" + option + "') is required.");
		}
	}
	else {
		return std::atoi(args.at(option).second.c_str());
	}
}

template<>
float App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	if (args.at(option).second == "") {
		// If optional, return default value
		if (get_args().at(option).first == App::Arg::OPTIONAL) {
			return std::stof(get_args().at(option).second.second.c_str());
		}
		else {
			throw std::invalid_argument("Option ('" + option + "') is required.");
		}
	}
	else {
		return std::stof(args.at(option).second.c_str());
	}
}

template<>
double App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	if (args.at(option).second == "") {
		// If optional, return default value
		if (get_args().at(option).first == App::Arg::OPTIONAL) {
			return std::stod(get_args().at(option).second.second.c_str());
		}
		else {
			throw std::invalid_argument("Option ('" + option + "') is required.");
		}
	}
	else {
		return std::stod(args.at(option).second.c_str());
	}
}

template<>
bool App::read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option) {
	return args.at(option).first;
}