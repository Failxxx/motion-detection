#pragma once

#ifdef _WIN32
#pragma warning(push, 0)
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>
#ifdef _WIN32
#pragma warning(pop)
#endif

class VideoManager {
public:
	~VideoManager() = default;

	bool initialize(const std::string& file_path);
	void manage_events(const int& key_pressed);

	bool set_video(const std::string& file_path);
	inline cv::VideoCapture& get_video() noexcept { return m_video; }

	inline uint get_width() { return m_width; }
	inline uint get_height() { return m_height; }
	inline uint get_length() { return m_length; }
	inline uint get_fps() { return m_fps; }

private:
	cv::VideoCapture m_video;

	uint m_width = 0;
	uint m_height = 0;
	uint m_length = 0;
	uint m_fps = 0;
};