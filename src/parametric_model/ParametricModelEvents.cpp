#include "ParametricModelEvents.hpp"
#include "ParametricModel.hpp"

void ParametricModelEvents::manage_events(App& app, const int& key_pressed) {
	if (!app.get_is_movie()) {
		app.get_camera().manage_events(key_pressed);		
	}
	else {
		app.get_movie().manage_events(key_pressed);
	}
    
	auto& model = dynamic_cast<ParametricModel&>(app);
	switch (key_pressed)
	{
		case App::Key::A:
			if (model.get_use_gmm()) {
				model.set_threshold(model.get_threshold() + 0.05f);
			} else {
				model.set_threshold(model.get_threshold() + 0.0005f);
			}
			break;

		case App::Key::Z:
			if (model.get_use_gmm()) {
				model.set_threshold(model.get_threshold() - 0.05f);
			} else {
				model.set_threshold(model.get_threshold() - 0.0005f);
			}
			break;

		case App::Key::T:
			std::cout << "Threshold value: " << model.get_threshold() << std::endl;
			break;

		case App::Key::ESC:
			app.set_run(false);
			break;

		default:
			break;
	}
}