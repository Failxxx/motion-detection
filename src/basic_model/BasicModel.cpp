#include "BasicModel.hpp"


/// Initialize the model
void BasicModel::initialize(const std::map<std::string, std::pair<bool, std::string>>& args) {
	// Read arguments
	try {
		m_camera_index = read_argument<int>(args, "-cam_idx");
		m_camera_api_preference = read_argument<int>(args, "-cam_api");
		m_camera_settings = read_argument<std::string>(args, "-cam_set");
		m_video_path = read_argument<std::string>(args, "-video");
		m_nb_frames_compute_background = read_argument<int>(args, "-bg");
		m_save = read_argument<bool>(args, "-save");
		m_output_name = read_argument<std::string>(args, "-o");
	}
	catch (const std::exception& exception) {
		std::cerr << "An error occured reading the list of options..." << std::endl;
		std::cerr << exception.what() << std::endl;
		m_run = false;
		return;
	}
	
	// Initialize model attributes
	m_name = "BasicModel";
	
	// Initialize the video manager or the camera manager
	bool initialized;
	if (m_video_path.empty()) {
		initialized = m_camera.initialize(m_camera_index, m_camera_settings, m_camera_api_preference);
	}
	else {
		initialized = m_movie.initialize(m_video_path);
		m_is_movie = true;
		// Check that the video contains enough frames to compute the backgound model
		if (m_nb_frames_compute_background >= m_movie.get_length()) {
			std::cerr << "You can't select more frames than the given video contains to compute a background model..." << std::endl;
			m_run = false;
			return;
		}
	}
	if (!initialized) { m_run = false; }

	// Initialize output videos
	if (m_save) {
		if (!initialize_outputs(m_output_name == "" ? m_name : m_output_name, 24)) {
			std::cerr << "Could not open the output videos for write." << std::endl;
			m_run = false;
			return;
		}
	}
}


/// Main function of the model.
void BasicModel::run() {
	// Exit before doing anything if something went wrong during initialization
	if (!m_run) { return; }

	uint frame_id;
	Chrono chrono;
	cv::Mat frame, foreground;
	cv::Mat kernel(8, 8, CV_8U, cv::Scalar(1, 1, 1));
	
	uint nb_frames = (m_is_movie) ? m_movie.get_length() : std::numeric_limits<uint>::max();
	int delay = (m_is_movie) ? std::ceil<int>(1000.0f / static_cast<float>(m_movie.get_fps())) : 1;

	// Get images to compute the background model
	for (frame_id = 0; frame_id < m_nb_frames_compute_background; ++frame_id) {
		if (!m_run) { break; }
		read_frame(frame);
		cv::cvtColor(frame, frame, cv::COLOR_RGB2GRAY);

		m_tmp_frames.push_back(frame.clone());

		m_events.manage_events(*this, cv::waitKey(delay));
	}

	if (m_run) {
		// Compute background model
		chrono.start();
		compute_background_model();
		chrono.stop();
		std::cout << "Compute background model: " << chrono.elapsed_time() << "s" << std::endl;

		// Release memory used to compute the model
		m_tmp_frames.clear();

		// Play the video from the beginning
		m_movie.get_video().set(cv::CAP_PROP_POS_FRAMES, 0);
	}

	double cumulated_time = 0.0;
	for (frame_id = 0; frame_id < nb_frames; ++frame_id) {
		if (!m_run) { break; }
		read_frame(frame);
		cv::cvtColor(frame, frame, cv::COLOR_RGB2GRAY);

		// Compute foreground
		chrono.start();
		cv::absdiff(frame, m_background, foreground);
		cv::threshold(foreground, foreground, 30, 255, cv::THRESH_BINARY);
		cv::morphologyEx(foreground, foreground, cv::MORPH_OPEN, kernel); // Remove part of noise
		chrono.stop();
		cumulated_time += chrono.elapsed_time();

		cv::imshow("Background model", m_background);
		cv::imshow(m_name, foreground);
		if (m_save) {
			write_outputs(frame, foreground);
		}
		m_events.manage_events(*this, cv::waitKey(delay));
	}

	if (frame_id > 0) {
		std::cout << "Compute foreground mean time per frame: " << cumulated_time / static_cast<double>(frame_id + 1) << "s" << std::endl;
	}
}


/// Function called when closing / stopping the programm.
void BasicModel::close() {
	if (m_save) {
		release_outputs();
	}
	cv::destroyAllWindows();
}


/// Basic method to compute a background model
void BasicModel::compute_background_model() {
	const uint width = m_tmp_frames[0].rows;
	const uint height = m_tmp_frames[0].cols;
	const uint channels = m_tmp_frames[0].channels();

	cv::Mat median_image = cv::Mat::zeros(cv::Size(height, width), CV_8U);

	std::vector<int> values;
	for (uint value_id = 0; value_id < width * height * channels; ++value_id) {

		for (auto& frame : m_tmp_frames) {
			values.push_back(frame.ptr()[value_id]);
		}

		median_image.ptr()[value_id] = compute_median(values);

		values.clear();
	}

	m_background = median_image;
}