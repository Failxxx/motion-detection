#pragma once

#include "../utils/include/App.hpp"
#include "BasicModelEvents.hpp"

class BasicModel : public App {
public:
	BasicModel() {};
	~BasicModel() noexcept {};

	void initialize(const std::map<std::string, std::pair<bool, std::string>>& args) override;
	void run() override;
	void close() override;

	// Singleton setup
	BasicModel(BasicModel& other) = delete;
	void operator=(BasicModel& singleton) = delete;
	inline static BasicModel& get() noexcept { return m_singleton; }


	const std::map<std::string, std::pair<App::Arg, std::pair<App::ArgType, std::string>>> get_args() const override { return this->m_args;	}
	
private:
	static BasicModel m_singleton;
	BasicModelEvents m_events;

	// Model methods
	void compute_background_model();

	// Model attributes
	std::vector<cv::Mat> m_tmp_frames;
	cv::Mat m_background;

	// Argument attributes
	int m_camera_index = 0;
	int m_camera_api_preference = cv::CAP_ANY; // cv::CAP_V4L = 200
	std::string m_camera_settings = "settings/UBUNTU_CameraSettings.json";
	std::string m_video_path = "";
	uint m_nb_frames_compute_background = 100;
	bool m_save = false;
	std::string m_output_name = "";
	const std::map<std::string, std::pair<App::Arg, std::pair<App::ArgType, std::string>>> m_args = {
		{"-cam_idx", {App::Arg::OPTIONAL, {App::ArgType::INT, "0"}}},										// Camera index
		{"-cam_api", {App::Arg::OPTIONAL, {App::ArgType::INT, "0"}}},										// Camera api preference
		{"-cam_set", {App::Arg::OPTIONAL, {App::ArgType::STRING, "settings/UBUNTU_CameraSettings.json"}}},	// Camera settings
		{"-video", {App::Arg::OPTIONAL, {App::ArgType::STRING, ""}}},										// Input file path
		{"-bg", {App::Arg::OPTIONAL, {App::ArgType::INT, "100"}}},											// Number of frames to compute the background model
		{"-save", {App::Arg::OPTIONAL, {App::ArgType::BOOL, ""}}},											// Whether to save generated images in a video
		{"-o", {App::Arg::OPTIONAL, {App::ArgType::STRING, ""}}},											// Output name
	};
};