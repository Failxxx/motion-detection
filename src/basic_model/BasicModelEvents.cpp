#include "BasicModelEvents.hpp"

void BasicModelEvents::manage_events(App& app, const int& key_pressed) {
	if (!app.get_is_movie()) {
		app.get_camera().manage_events(key_pressed);		
	}
	else {
		app.get_movie().manage_events(key_pressed);
	}
    
	switch (key_pressed)
	{
		case App::Key::ESC:
			app.set_run(false);
			break;

		default:
			break;
	}
}